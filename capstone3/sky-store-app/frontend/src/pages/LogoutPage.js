import React, { useEffect } from 'react'
import Logout from '../components/Login/Logout'
import loginStore from '../stores/loginStore'

const LogoutPage = () => {
  const store = loginStore();
  useEffect(() => {
    store.logout();
  });

  return (
    <div>
      <h1>You are now Log out.</h1>
      <Logout />
    </div>
  )
}

export default LogoutPage