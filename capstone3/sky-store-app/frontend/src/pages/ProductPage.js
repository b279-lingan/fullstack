import { useEffect } from "react";
import { Container, Row, Col } from 'react-bootstrap';
import productsStore from '../stores/productsStore';
import ProductTable from '../components/Products/ProductTable';
import UpdateProduct from '../components/Admin/Products/UpdateProduct';
import CreateProduct from '../components/Admin/Products/CreateProduct';

const ProductPage = () => {
  const store = productsStore();
  useEffect(() => {
    store.getAllProduct();
  });

  return (
    <div className="App">
      <Container className="mt-5">
        <Row>
          <Col xl={8} lg={8} md={8} sm={12} xs={12}>
            <ProductTable />
          </Col>
          <Col xl={4} lg={4} md={4} sm={12} xs={12}>
            <UpdateProduct />   
            <CreateProduct />            
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default ProductPage