import React from 'react'
import SidebarShop from '../components/Shop/SidebarShop'

const SidebarShopPage = () => {
  return (
    <div>
        <SidebarShop />
    </div>
  )
}

export default SidebarShopPage