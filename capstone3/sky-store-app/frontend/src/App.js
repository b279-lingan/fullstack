import AuthRegister from "./components/Auth/AuthRegister";
import LoginPage from "./pages/LoginPage";
import LogoutPage from "./pages/LogoutPage";
import ProductPage from "./pages/ProductPage";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import RegisterPage from "./pages/RegisterPage";
import HomePage from "./pages/HomePage";
import ContactPage from "./pages/ContactPage";
import AboutPage from "./pages/AboutPage";
import SampleProductPage from "./pages/SampleProductPage";
import SidebarShopPage from "./pages/SidebarShopPage";
import ShopDetailPage from "./pages/ShopDetailPage";
import CartPage from "./pages/CartPage";
import AdminPage from "./pages/AdminPage";
import AdminProductPage from "./pages/AdminProductPage";
import CreateProduct from "./components/Admin/Products/CreateProduct";
import Product1 from "./components/ProductDetails/Product1";
import Product2 from "./components/ProductDetails/Product2";
import Product3 from "./components/ProductDetails/Product3";
import Product4 from "./components/ProductDetails/Product4";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<HomePage/>}/>
          <Route path="/about" element={<AboutPage/>}/>
          <Route path="/product/cart" element={<CartPage/>}/>
          <Route path="/product/details" element={<ShopDetailPage/>}/>
          <Route path="/shop/sidebar" element={<SidebarShopPage/>}/>
          <Route path="/sample/product" element={<SampleProductPage/>}/>
          <Route path="/contact" element={<ContactPage/>}/>
          <Route path="/main/products" element={
            <AuthRegister>
              <ProductPage/>
            </AuthRegister>
          }/>
          <Route path="/login" element={<LoginPage/>}/>
          <Route path="/register" element={<RegisterPage />}/>
          <Route path="/logout" element={<LogoutPage />}/>

          <Route path="/admin/dashboard" element={<AdminPage/>}/>
          <Route path="/admin/product" element={<AdminProductPage/>}/>
          <Route path="/admin/create/product" element={<CreateProduct/>}/>
          <Route path="/product/6473c8b214cfb5a2d9453d25" element={<Product1/>}/>
          <Route path="/product/4tt67c8b214cfb5a2d9453d2" element={<Product2/>}/>
          <Route path="/product/76t67rtd214fb5a2d9453d2" element={<Product3/>}/>
          <Route path="/product/435sdr4td214fb5a2d9453d" element={<Product4/>}/>
          
        </Routes>
      </Router>
    </>
  );
}

export default App;
