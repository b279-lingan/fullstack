import React from 'react'
import { Nav } from 'react-bootstrap';

const Sidebar = () => {
  return (
    <div>
        <Nav className="flex-column bg-dark">
        <Nav.Link href="/">Home</Nav.Link>
        <Nav.Link href="/admin/product">Product</Nav.Link>
        <Nav.Link href="/account">Account</Nav.Link>
        <Nav.Link href="/logout">Logout</Nav.Link>
        </Nav>
    </div>
  )
}

export default Sidebar