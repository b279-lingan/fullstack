import React from 'react'
import { Table } from 'react-bootstrap';
import productsStore from '../../../stores/productsStore';
import ProductButton from './ProductButton';
import { Container, Row, Col } from 'react-bootstrap'; 
import Sidebar from '../Sidebar/Sidebar';

const Product = () => {
    const store = productsStore();
    return (
        <div>
            <Container fluid>
                <Row>
                    <Col md={3}>
                        <Sidebar />
                    </Col>
                    <Col md={9} className='mt-5'>
                        <h2 className="text-center">Product Table</h2>
                        <Table striped>
                            <thead>
                                <tr>
                                <th>Product</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>isAvailable</th>
                                <th className="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {store.products &&
                                store.products.map((product) => (
                                    <tr key={product._id}>
                                        <td>{product.product_name}</td>
                                        <td>{product.description}</td>
                                        <td>{product.price}</td>
                                        <td>{product.qty}</td>
                                        <td>
                                            <ProductButton product={product} key={product._id} />
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Product