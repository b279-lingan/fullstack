import React from 'react'
import { Form, Button } from 'react-bootstrap';
import productsStore from '../../../stores/productsStore';
import { FaEdit } from 'react-icons/fa';

const UpdateProduct = () => {
    const store = productsStore();
    if (!store.updateForm._id) return <></>

    return (
        <div>
            <h2>Update Product</h2>
            <Form onSubmit={store.updateProductData}>
            <Form.Group controlId="productName">
                <Form.Label>Product Name</Form.Label>
                <Form.Control
                placeholder="Enter Product Name"
                type="text"
                value={store.updateForm.product_name}
                onChange={store.handleUpdateFieldChange}
                name="product_name"
                />
            </Form.Group>
            <Form.Group controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control
                as="textarea"
                rows={3}
                placeholder="Enter description"
                onChange={store.handleUpdateFieldChange}
                value={store.updateForm.description}
                name="description"
                />
            </Form.Group>
            <Form.Group controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control
                type="number"
                placeholder="Enter price"
                onChange={store.handleUpdateFieldChange}
                value={store.updateForm.price}
                name="price"
                />
            </Form.Group>
            <Form.Group controlId="quantity">
                <Form.Label>Quantity</Form.Label>
                <Form.Control
                type="number"
                placeholder="Enter quantity"
                onChange={store.handleUpdateFieldChange}
                value={store.updateForm.qty}
                name="qty"
                />
            </Form.Group>
            <Button variant="primary" className="d-flex align-items-center mt-2" type="submit">
                <FaEdit className="me-2" />
                Update Product
            </Button>
            </Form>
        </div>
    )
}

export default UpdateProduct