import React from 'react'
import Sidebar from './Sidebar/Sidebar'
import Navigation from './Navigation/Navigation'
import { Container, Row, Col } from 'react-bootstrap';

const Admin = () => {
  return (
    <div>  
        <Container fluid>
            <Row>
                <Col md={3}>
                    <Sidebar />
                </Col>
                <Col md={9}>
                    <Navigation />
                </Col>
            </Row>
        </Container>
    </div>
  )
}

export default Admin