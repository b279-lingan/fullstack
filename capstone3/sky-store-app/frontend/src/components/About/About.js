import React from 'react';
import Footer from '../Footer/Footer';
import TopNavigation from '../Navigation/TopNavigation';
import MainHeader from '../Navigation/MainHeader';
import FooterCarousel from '../Footer/FooterCarousel';

import { Breadcrumb, Container, Col, Row, Image, Button } from 'react-bootstrap';

const About = () => {
  return (
    <>
      <TopNavigation />
      <MainHeader /> 

      <div className="all-title-box">
        <Container>
          <Row>
            <Col lg={12}>
              <h2>ABOUT US</h2>
              <Breadcrumb>
                <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
                <Breadcrumb.Item active>ABOUT US</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>
        </Container>
      </div>

      <div className="about-box-main">
        <Container>
          <Row>
            <Col lg={6}>
              <div className="banner-frame">
                <Image fluid src="" alt="" />
              </div>
            </Col>
            <Col lg={6}>
              <h2 className="noo-sh-title-top">We are <span>Skyshop</span></h2>
              <p>We are committed to succeeding. We will succeed if we make a commitment, set goals and execute a plan to accomplish those goals. This knowledge and action create a confident atmosphere within individuals, teams and the organization. To be the best alcohol company that sets high standards for responsible business and industry, with the products that are always number one for consumers in any category and every outlet.</p>
              <p>We fill brands with high quality and emotions to raise glasses full of cheer and good spirits.</p>
              <Button className="btn hvr-hover" href="#">Read More</Button>
            </Col>
          </Row>
        </Container>
      </div>
      <FooterCarousel />
      <Footer />
    </>
  )
}

export default About