import React from 'react'
import { Container, Row, Col, Form, Collapse, Nav, Carousel } from 'react-bootstrap';
import { FaUser, FaLocationArrow, FaHeadset, FaOpencart } from 'react-icons/fa';

const TopNavigation = () => {
  return (
    <div className='main-top'>
        <Container fluid>
            <Row>
                <Col lg={6} md={6} sm={12} xs={12}>
                    <div className='custom-select-box'>
                        <Form.Select id="basic" className="selectpicker show-tick" defaultValue="$ USD">
                            <option>¥ JPY</option>
							<option>$ USD</option>
							<option>€ EUR</option>
                        </Form.Select>
                    </div>
                    <div className="right-phone-box">
                        <Collapse in={true}>
                            <p>
                                Call US :- <a href="#"> +11 900 800 100</a>
                            </p>
                        </Collapse>
                    </div>
                    <div className="our-link">
                        <Nav>
                            <Nav.Link href="#">
                                <FaUser className="s_color" /> My Account
                            </Nav.Link>
                            <Nav.Link href="#">
                                <FaLocationArrow /> Our Location
                            </Nav.Link>
                            <Nav.Link href="#">
                                <FaHeadset /> Contact Us
                            </Nav.Link>
                        </Nav>
                    </div>
                </Col>
                <Col lg={6} md={6} sm={12} xs={12}>
                    <div className='login-box'>
                        <Form.Select id="basic" className="selectpicker show-tick" defaultValue="$ USD">
                            <option>Register Here</option>
							<option>Sign In</option>
                        </Form.Select>
                    </div>
                    <div id="offer-box" className="text-slid-box">
                        <Carousel className="carouselTicker">
                            <Carousel.Item>
                                <FaOpencart />
                                <span>20% off Entire Purchase Promo code: offT80</span>
                            </Carousel.Item>
                            <Carousel.Item>
                                <FaOpencart />
                                <span>50% - 80% off on Liqour</span>
                            </Carousel.Item>
                            <Carousel.Item>
                                <FaOpencart />
                                <span>Off 10%! Shop Liqour</span>
                            </Carousel.Item>
                            <Carousel.Item>
                                <FaOpencart />
                                <span>Off 50%! Shop Now</span>
                            </Carousel.Item>
                            <Carousel.Item>
                                <FaOpencart />
                                <span>Off 10%! Shop Liqour</span>
                            </Carousel.Item>
                            <Carousel.Item>
                                <FaOpencart />
                                <span>50% - 80% off on Liqour</span>
                            </Carousel.Item>
                            <Carousel.Item>
                                <FaOpencart />
                                <span>20% off Entire Purchase Promo code: offT30</span>
                            </Carousel.Item>
                            <Carousel.Item>
                                <FaOpencart />
                                <span>Off 50%! Shop Now</span>
                            </Carousel.Item>
                        </Carousel>
                    </div>
                </Col>
            </Row>
        </Container>
    </div>
  )
}

export default TopNavigation