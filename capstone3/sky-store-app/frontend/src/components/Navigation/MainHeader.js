import React from 'react'
import { Navbar, Nav, NavDropdown, Badge, Row, Col, Container, FormControl, Button } from 'react-bootstrap';
import { FaSearch, FaShoppingBag, FaShoppingCart } from 'react-icons/fa';
import { Link } from "react-router-dom";

const MainHeader = () => {
    return (
        <div>
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="/">
                        <img
                            src="/path/to/logo.png"
                            height="30"
                            className="d-inline-block align-top"
                            alt="Logo"
                        />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbar-nav" />
                    <Navbar.Collapse id="navbar-nav" className='justify-content-end'>
                        <Nav className="ml-auto">
                            <Nav.Link href="">
                                <Link to="/">Home</Link>
                            </Nav.Link>
                            <Nav.Link href="">
                                <Link to="/about">About Us</Link>
                            </Nav.Link>
                            <Nav.Link href="#link3">
                                <Link to="/sample/product">Product</Link>
                            </Nav.Link>
                            <Nav.Link href="#link3">
                                <Link to="/contact">Contact Us</Link>
                            </Nav.Link>
                        </Nav>
                        <Nav className="ml-auto">
                            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                            <Button variant="outline-primary">
                                <FaSearch />
                            </Button>
                            <Button variant="outline-primary">
                                <FaShoppingCart />
                            </Button>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}

export default MainHeader