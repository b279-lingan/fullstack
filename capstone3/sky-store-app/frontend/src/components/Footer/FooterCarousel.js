import React from 'react'

import { Carousel, Image  } from 'react-bootstrap';
import { FaInstagram } from 'react-icons/fa';

import footerImage1 from '../../assets/images/instagram-img-01.jpg';
import footerImage2 from '../../assets/images/instagram-img-02.jpg';
import footerImage3 from '../../assets/images/instagram-img-03.jpg';
import footerImage4 from '../../assets/images/instagram-img-04.jpg';
import footerImage5 from '../../assets/images/instagram-img-05.jpg';
import footerImage6 from '../../assets/images/instagram-img-06.jpg';
import footerImage7 from '../../assets/images/instagram-img-07.jpg';
import footerImage8 from '../../assets/images/instagram-img-08.jpg';
import footerImage9 from '../../assets/images/instagram-img-09.jpg';

const FooterCarousel = () => {
  return (
    <>
        <div className="instagram-box">
            <Carousel interval={null} fade={false}>
                <Carousel.Item>
                    <div className="d-flex">
                        <div className="ins-inner-box">
                            <Image fluid src={footerImage1} alt="Image 1" />
                            <div className="hov-in">
                                <a href="#"><FaInstagram /></a>
                            </div>
                        </div>
                        <div className="ins-inner-box">
                            <Image fluid src={footerImage2} alt="Image 2" />
                            <div className="hov-in">
                                <a href="#"><FaInstagram /></a>
                            </div>
                        </div>
                        <div className="ins-inner-box">
                            <Image fluid src={footerImage3} alt="Image 3" />
                            <div className="hov-in">
                                <a href="#"><FaInstagram /></a>
                            </div>
                        </div>
                        <div className="ins-inner-box">
                            <Image fluid src={footerImage4} alt="Image 4" />
                            <div className="hov-in">
                                <a href="#"><FaInstagram /></a>
                            </div>
                        </div>
                        <div className="ins-inner-box">
                            <Image fluid src={footerImage5} alt="Image 5" />
                            <div className="hov-in">
                                <a href="#"><FaInstagram /></a>
                            </div>
                        </div>
                        <div className="ins-inner-box">
                            <Image fluid src={footerImage6} alt="Image 6" />
                            <div className="hov-in">
                                <a href="#"><FaInstagram /></a>
                            </div>
                        </div>
                        <div className="ins-inner-box">
                            <Image fluid src={footerImage7} alt="Image 7" />
                            <div className="hov-in">
                                <a href="#"><FaInstagram /></a>
                            </div>
                        </div>
                        <div className="ins-inner-box">
                            <Image fluid src={footerImage8} alt="Image 8" />
                            <div className="hov-in">
                                <a href="#"><FaInstagram /></a>
                            </div>
                        </div>
                        <div className="ins-inner-box">
                            <Image fluid src={footerImage9} alt="Image 9" />
                            <div className="hov-in">
                                <a href="#"><FaInstagram /></a>
                            </div>
                        </div>
                    </div>
                </Carousel.Item>
            </Carousel>
        </div>
    </>
  )
}

export default FooterCarousel