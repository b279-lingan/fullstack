import React from 'react'
import { Container, Row, Col, Form, FormControl, InputGroup, Button } from 'react-bootstrap';
import { FaFacebook, FaTwitter, FaLinkedin, FaGooglePlus, FaRss, FaPinterestP, FaWhatsapp, FaMapMarkerAlt, FaPhoneSquare, FaEnvelope } from 'react-icons/fa';


const Footer = () => {
  return (
    <div>
        <footer>
            <div className="footer-main">
                <Container>
                    <Row>
                        <Col lg={4} md={12} sm={12}>
                            <div className="footer-top-box">
                                <h3>Business Time</h3>
                                <Container>
                                    <Row>
                                        <Col>
                                            <ul className="list-time">
                                                <li>Monday - Friday: 08:00am to 05:00am</li>
                                                <li>Saturday: 08:00am to 05:00am</li>
                                                <li>
                                                    Sunday: <span>08:00am to 07:00am</span>
                                                </li>
                                            </ul>
                                        </Col>
                                    </Row>
                                </Container>
                            </div>
                        </Col>
                        <Col lg={4} md={12} sm={12}>
                        </Col>
                        <Col lg={4} md={12} sm={12}>
                            <Row className="footer-top-box">
                                <Col>
                                    <h3>Social Media</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <ul>
                                        <li>
                                        <a href="#"><FaFacebook /></a>
                                        </li>
                                        <li>
                                        <a href="#"><FaTwitter /></a>
                                        </li>
                                        <li>
                                        <a href="#"><FaLinkedin /></a>
                                        </li>
                                        <li>
                                        <a href="#"><FaGooglePlus /></a>
                                        </li>
                                        <li>
                                        <a href="#"><FaRss /></a>
                                        </li>
                                        <li>
                                        <a href="#"><FaPinterestP /></a>
                                        </li>
                                        <li>
                                        <a href="#"><FaWhatsapp /></a>
                                        </li>
                                    </ul>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <hr />
                    <Row>
                        <Col lg={4} md={12} sm={12}>
                            <div className="footer-widget">
                                <h4>About Skyshop</h4>
                                <p>This is the cocktail mission also double as a restaurants with a great plates, burgers and fish and chips. They also dont go abroad with the nautical theme. So if your looking for a spot to dink, fuel up and meet on a patio under large floppin sail-or inside space that looks like the cabin of a wooden ships this place is it.</p> 							
                            </div>
                        </Col>
                        <Col lg={4} md={12} sm={12}>
                            <div className="footer-link">
                                <h4>Information</h4>
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Customer Service</a></li>
                                    <li><a href="#">Our Sitemap</a></li>
                                    <li><a href="#">Terms &amp; Conditions</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Delivery Information</a></li>
                                </ul>
                            </div>
                        </Col>
                        <Col lg={4} md={12} sm={12}>
                            <div className="footer-link-contact">
                                <h4>Contact Us</h4>
                                <ul>
                                <li>
                                    <p>
                                    <FaMapMarkerAlt />Address: Quezon City <br />Philippines,<br /> 1100
                                    </p>
                                </li>
                                <li>
                                    <p>
                                    <FaPhoneSquare />Phone: <a href="tel:+1-888705770">09152669126</a>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                    <FaEnvelope />Email: <a href="mailto:contactinfo@gmail.com">marlacamilelingan@gmail.com</a>
                                    </p>
                                </li>
                                </ul>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </footer>
    </div>
  )
}

export default Footer