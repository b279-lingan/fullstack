import React from 'react'
import Footer from '../Footer/Footer';
import TopNavigation from '../Navigation/TopNavigation';
import MainHeader from '../Navigation/MainHeader';
import FooterCarousel from '../Footer/FooterCarousel';

import { FaAngleLeft, FaAngleRight } from 'react-icons/fa';
import { Carousel, Breadcrumb, Container, Col, Row, Image, Form, Button } from 'react-bootstrap';
import { BsFillImageFill } from 'react-icons/bs';
import { FaHeart, FaSyncAlt, FaFacebook, FaGooglePlus, FaTwitter, FaPinterestP, FaWhatsapp } from 'react-icons/fa';
import galleryImage2 from '../../assets/images/img-pro-02.jpg';

const Product2 = () => {
  return (
    <div>
        <TopNavigation />
        <MainHeader /> 

        <div className="all-title-box">
            <Container>
            <Row>
                <Col lg={12}>
                <h2>PRODUCT DETAILS</h2>
                <Breadcrumb>
                    <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>PRODUCT DETAIL</Breadcrumb.Item>
                </Breadcrumb>
                </Col>
            </Row>
            </Container>
        </div>

        <div className="shop-detail-box-main">
            <Container>
                <Row>
                    <Col xl={5} lg={5} md={6}>
                        <Carousel>
                            <Carousel.Item>
                                <Image fluid src={galleryImage2} alt="" />
                            </Carousel.Item>
                            <Carousel.Item>
                                <div className="d-flex justify-content-center align-items-center">
                                    <BsFillImageFill size={150} />
                                </div>
                            </Carousel.Item>
                            <Carousel.Item>
                                <div className="d-flex justify-content-center align-items-center">
                                    <BsFillImageFill size={150} />
                                </div>
                            </Carousel.Item>
                        </Carousel>
                    </Col>
                    <Col xl={7} lg={7} md={6}>
                        <div className="single-product-details">
                            <h2>Cogniac</h2>
                            <h5>
                                <del>$56.00</del> $7.79
                            </h5>
                            <p className="available-stock">
                                <span>More than 20 available / <a href="#">8 sold</a></span>
                            </p>
                            <h4>Short Description:</h4>
                            <p>
                            Cognac production falls under French appellation d'origine contrôlée (AOC) designation, with production methods and naming required to meet certain legal requirements. Among the specified grapes, Ugni blanc, known locally as Saint-Émilion, is most widely used.
                            </p>
                            <ul>
                                <li>
                                <div className="form-group quantity-box">
                                    <label className="control-label">Quantity</label>
                                    <input className="form-control" value="0" min="0" max="20" type="number" />
                                </div>
                                </li>
                            </ul>

                            <div className="price-box-bar">
                                <div className="cart-and-bay-btn">
                                <Button className="btn hvr-hover" data-fancybox-close href="#">Buy New</Button>
                                <Button className="btn hvr-hover" data-fancybox-close href="#">Add to cart</Button>
                                </div>
                            </div>

                            <div className="add-to-btn">
                                <div className="add-comp">
                                <Button className="btn hvr-hover" href="#"><FaHeart /> Add to wishlist</Button>
                                <Button className="btn hvr-hover" href="#"><FaSyncAlt /> Add to Compare</Button>
                                </div>
                                <div className="share-bar">
                                <Button className="btn hvr-hover" href="#"><FaFacebook /></Button>
                                <Button className="btn hvr-hover" href="#"><FaGooglePlus /></Button>
                                <Button className="btn hvr-hover" href="#"><FaTwitter /></Button>
                                <Button className="btn hvr-hover" href="#"><FaPinterestP /></Button>
                                <Button className="btn hvr-hover" href="#"><FaWhatsapp /></Button>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>

        <FooterCarousel />
        <Footer />
    </div>
  )
}

export default Product2