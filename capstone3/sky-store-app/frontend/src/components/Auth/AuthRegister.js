import React, { useEffect } from 'react'
import loginStore from '../../stores/loginStore';
import { Navigate } from 'react-router-dom';

const AuthRegister = (props) => {

    const store = loginStore();
    useEffect(() => {
        if (store.loggedIn === null) {
            store.checkAuth()
        }
    });

    if (store.loggedIn === null) {
        return <div>Loading</div>
    }

    if(store.loggedIn === false) {
        // return <div>Please Login</div>
        return <Navigate to="/login" />
    }

    return (
        <div>{props.children}</div>
    )
}

export default AuthRegister