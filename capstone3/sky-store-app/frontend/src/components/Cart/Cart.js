import React from 'react'
import Footer from '../Footer/Footer';
import TopNavigation from '../Navigation/TopNavigation';
import MainHeader from '../Navigation/MainHeader';
import FooterCarousel from '../Footer/FooterCarousel';

import { Breadcrumb, Container, Col, Row, Table, Image, InputGroup, FormControl, Button } from 'react-bootstrap';

import categoriesImage1 from '../../assets/images/categories_img_01.jpg';
import categoriesImage2 from '../../assets/images/categories_img_02.jpg';
import categoriesImage3 from '../../assets/images/categories_img_03.jpg';

const Cart = () => {
  return (
    <div>
        <TopNavigation />
        <MainHeader /> 

        <div className="all-title-box">
            <Container>
            <Row>
                <Col lg={12}>
                <h2>Cart</h2>
                <Breadcrumb>
                    <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>Cart</Breadcrumb.Item>
                </Breadcrumb>
                </Col>
            </Row>
            </Container>
        </div>

        <div className="cart-box-main">
            <Container>
                <Row>
                    <Col lg={12}>
                        <div className="table-main table-responsive">
                            <Table className="table">
                                <thead>
                                <tr>
                                    <th>Images</th>
                                    <th>Product Name</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th>Remove</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td className="thumbnail-img">
                                    <a href="#">
                                        <Image className="img-fluid" src={categoriesImage1} alt="" />
                                    </a>
                                    </td>
                                    <td className="name-pr">
                                    <a href="#">
                                        Lorem ipsum dolor sit amet
                                    </a>
                                    </td>
                                    <td className="price-pr">
                                    <p>$ 80.0</p>
                                    </td>
                                    <td className="quantity-box">
                                    <InputGroup size="sm">
                                        <FormControl type="number" defaultValue="1" min="0" step="1" className="c-input-text qty text" />
                                    </InputGroup>
                                    </td>
                                    <td className="total-pr">
                                    <p>$ 80.0</p>
                                    </td>
                                    <td className="remove-pr">
                                    <Button variant="link">
                                        <i className="fas fa-times"></i>
                                    </Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="thumbnail-img">
                                    <a href="#">
                                        <Image className="img-fluid" src={categoriesImage2} alt="" />
                                    </a>
                                    </td>
                                    <td className="name-pr">
                                    <a href="#">
                                        Lorem ipsum dolor sit amet
                                    </a>
                                    </td>
                                    <td className="price-pr">
                                    <p>$ 60.0</p>
                                    </td>
                                    <td className="quantity-box">
                                    <InputGroup size="sm">
                                        <FormControl type="number" defaultValue="1" min="0" step="1" className="c-input-text qty text" />
                                    </InputGroup>
                                    </td>
                                    <td className="total-pr">
                                    <p>$ 80.0</p>
                                    </td>
                                    <td className="remove-pr">
                                    <Button variant="link">
                                        <i className="fas fa-times"></i>
                                    </Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="thumbnail-img">
                                    <a href="#">
                                        <Image className="img-fluid" src={categoriesImage3} alt="" />
                                    </a>
                                    </td>
                                    <td className="name-pr">
                                    <a href="#">
                                        Lorem ipsum dolor sit amet
                                    </a>
                                    </td>
                                    <td className="price-pr">
                                    <p>$ 30.0</p>
                                    </td>
                                    <td className="quantity-box">
                                    <InputGroup size="sm">
                                        <FormControl type="number" defaultValue="1" min="0" step="1" className="c-input-text qty text" />
                                    </InputGroup>
                                    </td>
                                    <td className="total-pr">
                                    <p>$ 80.0</p>
                                    </td>
                                    <td className="remove-pr">
                                    <Button variant="link">
                                        <i className="fas fa-times"></i>
                                    </Button>
                                    </td>
                                </tr>
                                </tbody>
                            </Table>
                        </div>
                    </Col>
                </Row>
                <Row className="my-5">
                    <Col lg={8} sm={12}></Col>
                    <Col lg={4} sm={12}>
                        <div className="order-box">
                        <h3>Order summary</h3>
                        <div className="d-flex">
                            <h4>Sub Total</h4>
                            <div className="ml-auto font-weight-bold"> $ 130 </div>
                        </div>
                        <div className="d-flex">
                            <h4>Discount</h4>
                            <div className="ml-auto font-weight-bold"> $ 40 </div>
                        </div>
                        <hr className="my-1" />
                        <div className="d-flex">
                            <h4>Coupon Discount</h4>
                            <div className="ml-auto font-weight-bold"> $ 10 </div>
                        </div>
                        <div className="d-flex">
                            <h4>Tax</h4>
                            <div className="ml-auto font-weight-bold"> $ 2 </div>
                        </div>
                        <div className="d-flex">
                            <h4>Shipping Cost</h4>
                            <div className="ml-auto font-weight-bold"> Free </div>
                        </div>
                        <hr />
                        <div className="d-flex gr-total">
                            <h5>Grand Total</h5>
                            <div className="ml-auto h5"> $ 388 </div>
                        </div>
                        <hr />
                        </div>
                    </Col>
                    <Col xs={12} className="d-flex shopping-box">
                        <Button href="checkout.html" className="ml-auto btn hvr-hover">
                        Checkout
                        </Button>
                    </Col>
                </Row>
            </Container>
        </div>

        <FooterCarousel />
        <Footer />
    </div>
  )
}

export default Cart