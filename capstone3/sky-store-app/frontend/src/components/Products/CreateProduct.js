import React from 'react'
import { Form, Button } from 'react-bootstrap';
import { FaPlus } from 'react-icons/fa';
import productsStore from '../../stores/productsStore';

const CreateProduct = () => {
    const store = productsStore();
    if (store.updateForm._id) return <></>

    return (
        <div>
            <h2>Create Product</h2>
            <Form onSubmit={store.createProduct}>
                <Form.Group controlId="productName">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control
                    placeholder="Enter Product Name"
                    type="text"
                    value={store.createForm.product_name}
                    onChange={store.updateCreateForm}
                    name="product_name"
                    />
                </Form.Group>
                <Form.Group controlId="description">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                    as="textarea"
                    rows={3}
                    placeholder="Enter description"
                    onChange={store.updateCreateForm}
                    value={store.createForm.description}
                    name="description"
                    />
                </Form.Group>
                <Form.Group controlId="price">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                    type="number"
                    placeholder="Enter price"
                    onChange={store.updateCreateForm}
                    value={store.createForm.price}
                    name="price"
                    />
                </Form.Group>
                <Form.Group controlId="quantity">
                    <Form.Label>Quantity</Form.Label>
                    <Form.Control
                    type="number"
                    placeholder="Enter quantity"
                    onChange={store.updateCreateForm}
                    value={store.createForm.qty}
                    name="qty"
                    />
                </Form.Group>
                <Button variant="primary" className="d-flex align-items-center mt-2" type="submit">
                    <FaPlus className="me-2" />
                    Add Product
                </Button>
            </Form>
        </div>
    )
}

export default CreateProduct