import React from 'react'
import { Table } from 'react-bootstrap';
import productsStore from '../../stores/productsStore';
import ProductButton from './ProductButton';

const ProductTable = () => {
    const store = productsStore();
    return (
        <div>
            <h2 className="text-center">Product Table</h2>
            <Table striped>
                <thead>
                    <tr>
                    <th>Product</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>isAvailable</th>
                    <th className="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {store.products &&
                    store.products.map((product) => (
                        <tr key={product._id}>
                            <td>{product.product_name}</td>
                            <td>{product.description}</td>
                            <td>{product.price}</td>
                            <td>{product.qty}</td>
                            <td>
                                <ProductButton product={product} key={product._id} />
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </div>
    )
}

export default ProductTable