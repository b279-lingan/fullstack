import React from 'react'
import productsStore from '../../stores/productsStore'

const ProductButton = ({product}) => {
    const store = productsStore((store) => {
        return {
            deleteProduct: store.deleteProduct,
            toggleUpdatedProduct: store.toggleUpdatedProduct
        }
    })

    return (
        <div className="d-flex justify-content-center">
            <button className="btn btn-info mx-1" onClick={() => store.toggleUpdatedProduct(product)}>Update</button>
            <button className="btn btn-danger mx-1" onClick={() => store.deleteProduct(product._id)}>Delete</button>
        </div>
    )
}

export default ProductButton