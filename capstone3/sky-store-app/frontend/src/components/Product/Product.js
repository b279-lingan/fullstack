import React from 'react'
import Footer from '../Footer/Footer';
import TopNavigation from '../Navigation/TopNavigation';
import MainHeader from '../Navigation/MainHeader';
import FooterCarousel from '../Footer/FooterCarousel';
import { FaEye, FaHeart, FaSyncAlt } from 'react-icons/fa';
import { Breadcrumb, Container, Col, Row, Image, Form, ButtonGroup, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';

import galleryImage1 from '../../assets/images/img-pro-01.jpg';
import galleryImage2 from '../../assets/images/img-pro-02.jpg';
import galleryImage3 from '../../assets/images/img-pro-03.jpg';
import galleryImage4 from '../../assets/images/img-pro-04.jpg';
import { Link } from "react-router-dom";

const Product = () => {
  return (
    <div>
        <TopNavigation />
        <MainHeader /> 

        <div className="all-title-box">
            <Container>
                <Row>
                    <Col lg={12}>
                    <h2>OUR PRODUCTS</h2>
                    <Breadcrumb>
                        <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
                        <Breadcrumb.Item active>PRODUCTS</Breadcrumb.Item>
                    </Breadcrumb>
                    </Col>
                </Row>
            </Container>
        </div>

        <div className="products-box">
            <Container>
                <Row>
                    <Col lg={12}>
                        <div class="title-all text-center">
                            <h1>Our Gallery</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet lacus enim.</p>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12}>
                    <div className="special-menu text-center">
                        <ButtonGroup className="filter-button-group">
                        <Button variant="primary" active data-filter="*">All</Button>
                        <Button variant="primary" data-filter=".bulbs">Top Featured</Button>
                        <Button variant="primary" data-filter=".fruits">Best Seller</Button>
                        </ButtonGroup>
                    </div>
                    </Col>
                </Row>
                <Row className='special-list'>
                    <Col lg={3} md={6} className="special-grid bulbs">
                        <div className="products-single fix">
                            <div className="box-img-hover">
                            <div className="type-lb">
                                <p className="sale">Sale</p>
                            </div>
                            <Image src={galleryImage1} fluid alt="Image" />
                            <div className="mask-icon">
                                <ul>
                                <OverlayTrigger
                                    placement="right"
                                    overlay={<Tooltip id="tooltip-view">View</Tooltip>}
                                >
                                    <li>
                                    <a href="#">
                                        <i className="fas fa-eye"></i>
                                    </a>
                                    </li>
                                </OverlayTrigger>
                                <OverlayTrigger
                                    placement="right"
                                    overlay={<Tooltip id="tooltip-compare">Compare</Tooltip>}
                                >
                                    <li>
                                    <a href="#">
                                        <i className="fas fa-sync-alt"></i>
                                    </a>
                                    </li>
                                </OverlayTrigger>
                                <OverlayTrigger
                                    placement="right"
                                    overlay={<Tooltip id="tooltip-wishlist">Add to Wishlist</Tooltip>}
                                >
                                    <li>
                                    <a href="#">
                                        <i className="far fa-heart"></i>
                                    </a>
                                    </li>
                                </OverlayTrigger>
                                </ul>
                                <a className="cart" href="/">
                                    <Link to="/product/6473c8b214cfb5a2d9453d25">Add to Cart</Link>
                                </a>
                            </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg={3} md={6} className="special-grid bulbs">
                        <div className="products-single fix">
                            <div className="box-img-hover">
                            <div className="type-lb">
                                <p className="sale">Sale</p>
                            </div>
                            <Image src={galleryImage2} fluid alt="Image" />
                            <div className="mask-icon">
                                <ul>
                                    <OverlayTrigger
                                        placement="right"
                                        overlay={<Tooltip id="tooltip-view">View</Tooltip>}
                                    >
                                        <li>
                                            <a href="#">
                                                <FaEye />
                                            </a>
                                        </li>
                                    </OverlayTrigger>
                                    <OverlayTrigger
                                        placement="right"
                                        overlay={<Tooltip id="tooltip-compare">Compare</Tooltip>}
                                    >
                                        <li>
                                            <a href="#">
                                                <FaSyncAlt />
                                            </a>
                                        </li>
                                    </OverlayTrigger>
                                    <OverlayTrigger
                                        placement="right"
                                        overlay={<Tooltip id="tooltip-wishlist">Add to Wishlist</Tooltip>}
                                    >
                                        <li>
                                            <a href="#">
                                                <FaHeart />
                                            </a>
                                        </li>
                                    </OverlayTrigger>
                                </ul>
                                <a className="cart" href="/">
                                    <Link to="/product/4tt67c8b214cfb5a2d9453d2">Add to Cart</Link>
                                </a>
                            </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg={3} md={6} className="special-grid bulbs">
                        <div className="products-single fix">
                            <div className="box-img-hover">
                            <div className="type-lb">
                                <p className="sale">Sale</p>
                            </div>
                            <Image src={galleryImage3} fluid alt="Image" />
                            <div className="mask-icon">
                                <ul>
                                    <OverlayTrigger
                                        placement="right"
                                        overlay={<Tooltip id="tooltip-view">View</Tooltip>}
                                    >
                                        <li>
                                            <a href="#">
                                                <FaEye />
                                            </a>
                                        </li>
                                    </OverlayTrigger>
                                    <OverlayTrigger
                                        placement="right"
                                        overlay={<Tooltip id="tooltip-compare">Compare</Tooltip>}
                                    >
                                        <li>
                                            <a href="#">
                                                <FaSyncAlt />
                                            </a>
                                        </li>
                                    </OverlayTrigger>
                                    <OverlayTrigger
                                        placement="right"
                                        overlay={<Tooltip id="tooltip-wishlist">Add to Wishlist</Tooltip>}
                                    >
                                        <li>
                                            <a href="#">
                                                <FaHeart />
                                            </a>
                                        </li>
                                    </OverlayTrigger>
                                </ul>
                                <a className="cart" href="/">
                                    <Link to="/product/76t67rtd214fb5a2d9453d2">Add to Cart</Link>
                                </a>
                            </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg={3} md={6} className="special-grid bulbs">
                        <div className="products-single fix">
                            <div className="box-img-hover">
                            <div className="type-lb">
                                <p className="sale">Sale</p>
                            </div>
                            <Image src={galleryImage4} fluid alt="Image" />
                            <div className="mask-icon">
                                <ul>
                                    <OverlayTrigger
                                        placement="right"
                                        overlay={<Tooltip id="tooltip-view">View</Tooltip>}
                                    >
                                        <li>
                                            <a href="#">
                                                <FaEye />
                                            </a>
                                        </li>
                                    </OverlayTrigger>
                                    <OverlayTrigger
                                        placement="right"
                                        overlay={<Tooltip id="tooltip-compare">Compare</Tooltip>}
                                    >
                                        <li>
                                            <a href="#">
                                                <FaSyncAlt />
                                            </a>
                                        </li>
                                    </OverlayTrigger>
                                    <OverlayTrigger
                                        placement="right"
                                        overlay={<Tooltip id="tooltip-wishlist">Add to Wishlist</Tooltip>}
                                    >
                                        <li>
                                            <a href="#">
                                                <FaHeart />
                                            </a>
                                        </li>
                                    </OverlayTrigger>
                                </ul>
                                <a className="cart" href="/">
                                    <Link to="/product/435sdr4td214fb5a2d9453d">Add to Cart</Link>
                                </a>
                            </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>

        <FooterCarousel />
        <Footer />
    </div>
  )
}

export default Product