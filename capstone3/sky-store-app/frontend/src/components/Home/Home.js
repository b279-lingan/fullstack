import React, { useState } from 'react';
import TopNavigation from '../Navigation/TopNavigation';
import MainHeader from '../Navigation/MainHeader';
import { Carousel, Container, Image, Row, Col, ButtonGroup, Button, Tooltip, OverlayTrigger  } from 'react-bootstrap';
import { FaAngleLeft, FaAngleRight, FaEye, FaSyncAlt, FaHeart, FaComments, FaInstagram } from 'react-icons/fa';
import Footer from '../Footer/Footer';
import MainCarousel from '../Carousel/MainCarousel';
import { Link } from "react-router-dom";

// IMAGES
import categoriesImage1 from '../../assets/images/categories_img_01.jpg';
import categoriesImage2 from '../../assets/images/categories_img_02.jpg';
import categoriesImage3 from '../../assets/images/categories_img_03.jpg';
import bannerImage1 from '../../assets/images/add-img-01.jpg';
import bannerImage2 from '../../assets/images/add-img-02.jpg';
import galleryImage1 from '../../assets/images/img-pro-01.jpg';
import galleryImage2 from '../../assets/images/img-pro-02.jpg';
import galleryImage3 from '../../assets/images/img-pro-03.jpg';
import galleryImage4 from '../../assets/images/img-pro-04.jpg';
import blogImage1 from '../../assets/images/blog-img-01.jpg';
import blogImage2 from '../../assets/images/blog-img-02.jpg';
import blogImage3 from '../../assets/images/blog-img-03.jpg';
import FooterCarousel from '../Footer/FooterCarousel';

const Home = () => {
    const renderTooltip = (text) => (
        <Tooltip id="tooltip">{text}</Tooltip>
    );
    const [tooltipPlacement, setTooltipPlacement] = useState('top');
    return (
        <>
            <TopNavigation />
            <MainHeader />
            <MainCarousel />
            <Container className="categories-shop">
                <Row>
                    <Col lg={4} md={4} sm={12} xs={12}>
                        <div className="shop-cat-box">
                            <Image className="d-block w-100" src={categoriesImage1} alt="Image 1" />
                            <Button className="btn hvr-hover" href="#">
                            </Button>
                        </div>
                    </Col>
                    <Col lg={4} md={4} sm={12} xs={12}>
                        <div className="shop-cat-box">
                            <Image className="d-block w-100" src={categoriesImage2} alt="Image 1" />
                            <Button className="btn hvr-hover" href="#">
                            </Button>
                        </div>
                    </Col>
                    <Col lg={4} md={4} sm={12} xs={12}>
                        <div className="shop-cat-box">
                            <Image className="d-block w-100" src={categoriesImage3} alt="Image 1" />
                            <Button className="btn hvr-hover" href="#">
                            </Button>
                        </div>
                    </Col>
                </Row>
            </Container>
            <div className="box-add-products">
                <Container>
                    <Row>
                        <Col lg={6} md={6} sm={12}>
                            <div className="offer-box-products">
                                <Image fluid src={bannerImage1} alt="" />
                            </div>
                        </Col>
                        <Col lg={6} md={6} sm={12}>
                            <div className="offer-box-products">
                                <Image fluid src={bannerImage2} alt="" />
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
            <Container className='mt-3'>
                <Row>
                    <Col lg={12}>
                        <h1>Alcoholic beverage</h1>
                        <p> a drink that contains ethanol, a type of alcohol that acts as a drug and is produced by fermentation of grains, fruits, or other sources of sugar.</p>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12}>
                        <div className="special-menu text-center">
                            <ButtonGroup className="filter-button-group">
                                <Button className="active" data-filter="*">All</Button>
                                <Button data-filter=".top-featured">Top featured</Button>
                                <Button data-filter=".best-seller">Best seller</Button>
                            </ButtonGroup>
                        </div>
                    </Col>
                </Row>
                <Row className="special-list">
                    <Col lg={3} md={6} className="special-grid best-seller">
                        <div className="products-single fix">
                            <div className="box-img-hover">
                                <div className="type-lb">
                                    <p className="sale">Sale</p>
                                </div>
                                <Image fluid src={galleryImage1} alt="" />
                                <div className="mask-icon">
                                <div className="mask-icon">
                                    <ul>
                                        <li>
                                            <OverlayTrigger key="view" placement={tooltipPlacement} overlay={<Tooltip id="view-tooltip">View</Tooltip>}>
                                                <a href="#">
                                                    <FaEye />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                        <li>
                                            <OverlayTrigger key="compare" placement={tooltipPlacement} overlay={<Tooltip id="compare-tooltip">Compare</Tooltip>}>
                                                <a href="#">
                                                    <FaSyncAlt />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                        <li>
                                            <OverlayTrigger key="wishlist" placement={tooltipPlacement} overlay={<Tooltip id="wishlist-tooltip">Add to Wishlist</Tooltip>}>
                                                <a href="#">
                                                    <FaHeart />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                    </ul>
                                    <a className="cart" href="/">
                                        <Link to="/product/6473c8b214cfb5a2d9453d25">Add to Cart</Link> 
                                    </a>
                                </div>
                                </div>
                                <div className="why-text">
                                    <h4>Beer</h4>
                                    <h5>$7.79</h5>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg={3} md={6} className="special-grid best-seller">
                        <div className="products-single fix">
                            <div className="box-img-hover">
                                <div className="type-lb">
                                    <p className="sale">Sale</p>
                                </div>
                                <Image fluid src={galleryImage2} alt="" />
                                <div className="mask-icon">
                                <div className="mask-icon">
                                    <ul>
                                        <li>
                                            <OverlayTrigger key="view" placement={tooltipPlacement} overlay={<Tooltip id="view-tooltip">View</Tooltip>}>
                                                <a href="#">
                                                    <FaEye />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                        <li>
                                            <OverlayTrigger key="compare" placement={tooltipPlacement} overlay={<Tooltip id="compare-tooltip">Compare</Tooltip>}>
                                                <a href="#">
                                                    <FaSyncAlt />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                        <li>
                                            <OverlayTrigger key="wishlist" placement={tooltipPlacement} overlay={<Tooltip id="wishlist-tooltip">Add to Wishlist</Tooltip>}>
                                                <a href="#">
                                                    <FaHeart />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                    </ul>
                                    <a className="cart" href="/">
                                    <Link to="/product/4tt67c8b214cfb5a2d9453d2">Add to Cart</Link>
                                    </a>
                                </div>
                                </div>
                                <div className="why-text">
                                    <h4>Cogniac</h4>
                                    <h5>$7.79</h5>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg={3} md={6} className="special-grid best-seller">
                        <div className="products-single fix">
                            <div className="box-img-hover">
                                <div className="type-lb">
                                    <p className="sale">Sale</p>
                                </div>
                                <Image fluid src={galleryImage3} alt="" />
                                <div className="mask-icon">
                                <div className="mask-icon">
                                    <ul>
                                        <li>
                                            <OverlayTrigger key="view" placement={tooltipPlacement} overlay={<Tooltip id="view-tooltip">View</Tooltip>}>
                                                <a href="#">
                                                    <FaEye />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                        <li>
                                            <OverlayTrigger key="compare" placement={tooltipPlacement} overlay={<Tooltip id="compare-tooltip">Compare</Tooltip>}>
                                                <a href="#">
                                                    <FaSyncAlt />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                        <li>
                                            <OverlayTrigger key="wishlist" placement={tooltipPlacement} overlay={<Tooltip id="wishlist-tooltip">Add to Wishlist</Tooltip>}>
                                                <a href="#">
                                                    <FaHeart />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                    </ul>
                                    <a className="cart" href="/">
                                        <Link to="/product/76t67rtd214fb5a2d9453d2">Add to Cart</Link>
                                    </a>
                                </div>
                                </div>
                                <div className="why-text">
                                    <h4>Gin</h4>
                                    <h5>$7.79</h5>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg={3} md={6} className="special-grid best-seller">
                        <div className="products-single fix">
                            <div className="box-img-hover">
                                <div className="type-lb">
                                    <p className="sale">Sale</p>
                                </div>
                                <Image fluid src={galleryImage4} alt="" />
                                <div className="mask-icon">
                                <div className="mask-icon">
                                    <ul>
                                        <li>
                                            <OverlayTrigger key="view" placement={tooltipPlacement} overlay={<Tooltip id="view-tooltip">View</Tooltip>}>
                                                <a href="#">
                                                    <FaEye />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                        <li>
                                            <OverlayTrigger key="compare" placement={tooltipPlacement} overlay={<Tooltip id="compare-tooltip">Compare</Tooltip>}>
                                                <a href="#">
                                                    <FaSyncAlt />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                        <li>
                                            <OverlayTrigger key="wishlist" placement={tooltipPlacement} overlay={<Tooltip id="wishlist-tooltip">Add to Wishlist</Tooltip>}>
                                                <a href="#">
                                                    <FaHeart />
                                                </a>
                                            </OverlayTrigger>
                                        </li>
                                    </ul>
                                    <a className="cart" href="/">
                                        <Link to="/product/435sdr4td214fb5a2d9453d">Add to Cart</Link>
                                    </a>
                                </div>
                                </div>
                                <div className="why-text">
                                    <h4>Rhum</h4>
                                    <h5>$7.79</h5>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
            <div className="latest-blog">
                <Container>
                    <Row>
                        <Col lg={12}>
                            <h1>Latest Blog</h1>
                            <p>To delight and nourish our customers with healthy, quality and delicious food and excellent service at a reasonable price.</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6} lg={4} xl={4}>
                            <div className='blog-box'>
                                <div className="blog-img">
                                    <Image fluid src={blogImage1} alt="" />
                                </div>
                                <div className="blog-content">
                                    <div className="title-blog">
                                        <h3>Vodka</h3>
                                        <p>Vodka is traditionally drunk "neat" (not mixed with water, ice, or other mixers), and it is often served freezer chilled in the vodka belt of Belarus, Estonia, Finland, Iceland, Latvia, Lithuania, Norway, Poland, Russia, Sweden, and Ukraine.</p>
                                    </div>
                                    <ul className="option-blog">
                                        <li>
                                            <a href="#">
                                                <FaHeart className="far" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <FaEye className="fas" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <FaComments className="far" />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </Col>
                        <Col md={6} lg={4} xl={4}>
                            <div className='blog-box'>
                                <div className="blog-img">
                                    <Image fluid src={blogImage2} alt="" />
                                </div>
                                <div className="blog-content">
                                    <div className="title-blog">
                                        <h3>Bourbon</h3>
                                        <p>is a type of barrel-aged American whiskey made primarily from corn (maize). The name derives from the French Bourbon dynasty, although the precise source of inspiration is uncertain; contenders include Bourbon County in Kentucky and Bourbon Street in New Orleans, both of which are named after the dynasty.</p>
                                    </div>
                                    <ul className="option-blog">
                                        <li>
                                            <a href="#">
                                                <FaHeart className="far" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <FaEye className="fas" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <FaComments className="far" />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </Col>
                        <Col md={6} lg={4} xl={4}>
                            <div className='blog-box'>
                                <div className="blog-img">
                                    <Image fluid src={blogImage3} alt="" />
                                </div>
                                <div className="blog-content">
                                    <div className="title-blog">
                                        <h3>Wine</h3>
                                        <p>is an alcoholic drink typically made from fermented grapes. Yeast consumes the sugar in the grapes and converts it to ethanol and carbon dioxide, releasing heat in the process. Different varieties of grapes and strains of yeasts are major factors in different styles of wine.</p>
                                    </div>
                                    <ul className="option-blog">
                                        <li>
                                            <a href="#">
                                                <FaHeart className="far" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <FaEye className="fas" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <FaComments className="far" />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
            <FooterCarousel />
            <Footer />
        </>
    )
}

export default Home