import React from 'react'
import loginStore from '../../stores/loginStore';
import { useNavigate } from 'react-router-dom';
import Footer from '../Footer/Footer';
import TopNavigation from '../Navigation/TopNavigation';
import MainHeader from '../Navigation/MainHeader';
import FooterCarousel from '../Footer/FooterCarousel';
import Swal from 'sweetalert2';

import { Breadcrumb, Container, Col, Row, Form, Button } from 'react-bootstrap';
import { FaMapMarkerAlt, FaPhoneSquare, FaEnvelope } from 'react-icons/fa';

const Login = () => {

  const store = loginStore();
  // const navigate = useNavigate();

  const handleLogin = async (e) => {
    e.preventDefault();
    // await store.login();

    Swal.fire({
      title: 'Success!',
      text: 'Welcome to Sky E-Store',
      icon: 'success',
      confirmButtonText: 'Cool'
    }).then(() => {
      window.location.href = "/";
    });
  }  
  
  return (
    <div>
      <TopNavigation />
      <MainHeader /> 

      <div className="all-title-box">
        <Container>
          <Row>
            <Col lg={12}>
              <h2>Login</h2>
              <Breadcrumb>
                <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
                <Breadcrumb.Item active>Login</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>
        </Container>
      </div>

      <div className="contact-box-main">
        <Container>
          <Row>
            <Col lg={8} sm={12}>
              <div className="contact-form-right">
                <h2>GET IN TOUCH</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed odio justo, ultrices ac nisl sed, lobortis porta elit. Fusce in metus ac ex venenatis ultricies at cursus mauris.
                </p>
                <Form id="contactForm" onSubmit={handleLogin}>
                  <Form.Group className='mb-2' controlId="email">
                    <Form.Control type="email" name="email" placeholder="Your Email" onChange={store.updateLoginForm} value={store.loginForm.email} required />
                  </Form.Group>
                  <Form.Group className='mb-2' controlId="subject">
                    <Form.Control type="password" name="password" placeholder="Subject" onChange={store.updateLoginForm} value={store.loginForm.password} required />
                  </Form.Group>
                  <div className="submit-button text-center">
                    <Button className="btn hvr-hover" type="submit">Login</Button>
                    <div id="msgSubmit" className="h3 text-center hidden"></div>
                    <div className="clearfix"></div>
                  </div>
                </Form>
              </div>
            </Col>
            <Col lg={4} sm={12}>
              <div className="contact-info-left">
                <h2>CONTACT INFO</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna
                  diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo
                  sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.
                </p>
                <ul>
                  <li>
                    <p>
                      <FaMapMarkerAlt /> Address: Michael I. Days 9000 <br />
                      Preston Street Wichita, <br />
                      KS 87213
                    </p>
                  </li>
                  <li>
                    <p>
                      <FaPhoneSquare /> Phone: <a href="tel:+1-888705770">+1-888 705 770</a>
                    </p>
                  </li>
                  <li>
                    <p>
                      <FaEnvelope /> Email:{' '}
                      <a href="mailto:contactinfo@gmail.com">contactinfo@gmail.com</a>
                    </p>
                  </li>
                </ul>
              </div>
            </Col>
          </Row>
        </Container>
      </div>

      <FooterCarousel />
      <Footer />
    </div>
  )
}

export default Login