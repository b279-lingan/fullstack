import React from 'react';
import { Carousel,  Container, Image, Row, Col  } from 'react-bootstrap';
import carouselImage1 from '../../assets/images/banner-01.jpg';
import carouselImage2 from '../../assets/images/banner-02.jpg';
import carouselImage3 from '../../assets/images/banner-03.jpg';

const MainCarousel = () => {
    return (
        <div>
            <Carousel>
                <Carousel.Item>
                    <Image className="d-block w-100" src={carouselImage1} alt="Image 1" />
                    <Carousel.Caption>
                        <Container className='bg-red'>
                            <Row>
                                <Col md={12}>
                                    <h1 className="m-b-20"><strong>Welcome To <br /> Skyshop</strong></h1>
                                    <p className="m-b-40">See how your users experience your website in realtime or view <br /> trends to see any changes in performance over time.</p>
                                    <p><a className="btn hvr-hover" href="#">Shop New</a></p>
                                </Col>
                            </Row>
                        </Container>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <Image className="d-block w-100" src={carouselImage2} alt="Image 2" />
                    <Carousel.Caption>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <h1 className="m-b-20"><strong>Welcome To <br /> Skyshop</strong></h1>
                                    <p className="m-b-40">See how your users experience your website in realtime or view <br /> trends to see any changes in performance over time.</p>
                                    <p><a className="btn hvr-hover" href="#">Shop New</a></p>
                                </div>
                            </div>
                        </div>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <Image className="d-block w-100" src={carouselImage3} alt="Image 2" />
                    <Carousel.Caption>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <h1 className="m-b-20"><strong>Welcome To <br /> Skyshop</strong></h1>
                                    <p className="m-b-40">See how your users experience your website in realtime or view <br /> trends to see any changes in performance over time.</p>
                                    <p><a className="btn hvr-hover" href="#">Shop New</a></p>
                                </div>
                            </div>
                        </div>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </div>
    )
}

export default MainCarousel