import React from 'react'
import Footer from '../Footer/Footer';
import TopNavigation from '../Navigation/TopNavigation';
import MainHeader from '../Navigation/MainHeader';
import FooterCarousel from '../Footer/FooterCarousel';

import { Breadcrumb, Container, Col, Row, Image, Form, Button } from 'react-bootstrap';
import { FaMapMarkerAlt, FaPhoneSquare, FaEnvelope } from 'react-icons/fa';

const Contact = () => {
  return (
    <div>
        <TopNavigation />
        <MainHeader /> 

        <div className="all-title-box">
            <Container>
            <Row>
                <Col lg={12}>
                <h2>CONTACT US</h2>
                <Breadcrumb>
                    <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>CONTACT US</Breadcrumb.Item>
                </Breadcrumb>
                </Col>
            </Row>
            </Container>
        </div>

        <div className="contact-box-main">
            <Container>
                <Row>
                    <Col lg={8} sm={12}>
                        <div className="contact-form-right">
                            <h2>GET IN TOUCH</h2>
                            <p>
                            To serve happiness to our customers through delicious, quality meals and extraordinary restaurant experience while working toward the greater good for our employees, community and environment.
                            </p>
                            <Form id="contactForm">
                                <Form.Group className='mb-2' controlId="name">
                                    <Form.Control type="text" placeholder="Your Name" required />
                                </Form.Group>
                                <Form.Group className='mb-2' controlId="email">
                                    <Form.Control type="email" placeholder="Your Email" required />
                                </Form.Group>
                                <Form.Group className='mb-2' controlId="subject">
                                    <Form.Control type="text" placeholder="Subject" required />
                                </Form.Group>
                                <Form.Group className='mb-2' controlId="message">
                                    <Form.Control as="textarea" rows={4} placeholder="Your Message" required />
                                </Form.Group>
                                <div className="submit-button text-center">
                                    <Button className="btn hvr-hover" type="submit">Send Message</Button>
                                    <div id="msgSubmit" className="h3 text-center hidden"></div>
                                    <div className="clearfix"></div>
                                </div>
                            </Form>
                        </div>
                    </Col>
                    <Col lg={4} sm={12}>
                        <div className="contact-info-left">
                            <h2>CONTACT INFO</h2>
                            <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent urna
                            diam, maximus ut ullamcorper quis, placerat id eros. Duis semper justo
                            sed condimentum rutrum. Nunc tristique purus turpis. Maecenas vulputate.
                            </p>
                            <ul>
                            <li>
                                <p>
                                <FaMapMarkerAlt /> Address: Michael I. Days 9000 <br />
                                Preston Street Wichita, <br />
                                KS 87213
                                </p>
                            </li>
                            <li>
                                <p>
                                <FaPhoneSquare /> Phone: <a href="tel:+1-888705770">+1-888 705 770</a>
                                </p>
                            </li>
                            <li>
                                <p>
                                <FaEnvelope /> Email:{' '}
                                <a href="mailto:contactinfo@gmail.com">contactinfo@gmail.com</a>
                                </p>
                            </li>
                            </ul>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>

        <FooterCarousel />
        <Footer />
    </div>
  )
}

export default Contact