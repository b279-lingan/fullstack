import React from 'react'
import Footer from '../Footer/Footer';
import TopNavigation from '../Navigation/TopNavigation';
import MainHeader from '../Navigation/MainHeader';
import FooterCarousel from '../Footer/FooterCarousel';

import { Breadcrumb, Container, Col, Row, Image, Form, Button, Card, InputGroup, Nav, Tab, Tabs, FormControl, Collapse, ListGroup } from 'react-bootstrap';
import { FaShoppingCart, FaTh, FaListUl, FaEye, FaSyncAlt, FaHeart, FaSearch } from 'react-icons/fa';
import { BsChevronDown, BsPlus } from 'react-icons/bs';

const SidebarShop = () => {
  return (
    <div>
        <TopNavigation />
        <MainHeader />

        <div className="all-title-box">
            <Container>
            <Row>
                <Col lg={12}>
                <h2>OUR STORE</h2>
                <Breadcrumb>
                    <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>STORE</Breadcrumb.Item>
                </Breadcrumb>
                </Col>
            </Row>
            </Container>
        </div>

        <div className="shop-box-inner">
            <Container>
                <Row>
                    <Col xl={9} lg={9} sm={12} xs={12} className="shop-content-right">
                        <div className="right-product-box">
                            <Row className="product-item-filter">
                                <span>Sort by </span>
                                <Col xs={12} sm={8} className="text-center text-sm-left">
                                    <div className="toolbar-sorter-right">
                                        <Form>
                                            <Form.Group controlId="basic">
                                                <InputGroup>
                                                    <Form.Control as="select" className="selectpicker show-tick form-control" data-placeholder="$ USD">
                                                        <option data-display="Select">Nothing</option>
                                                        <option value="1">Popularity</option>
                                                        <option value="2">High Price → High Price</option>
                                                        <option value="3">Low Price → High Price</option>
                                                        <option value="4">Best Selling</option>
                                                    </Form.Control>
                                                </InputGroup>
                                            </Form.Group>
                                        </Form>
                                    </div>
                                    <p>Showing all 4 results</p>
                                </Col>
                            </Row>
                            <div className="product-categorie-box">
                                <Tabs defaultActiveKey="tab-1" id="product-categorie-tab" className="tab-content">
                                    <Tab eventKey="tab-1" title={<><FaTh /></>} id="grid-view">
                                        <Row>
                                            <Col sm={6} md={6} lg={4} xl={4}>
                                                <Card className="products-single">
                                                    <div className="box-img-hover">
                                                        <div className="type-lb">
                                                            <p className="sale">Sale</p>
                                                        </div>
                                                        <Card.Img src="images/img-pro-01.jpg" fluid alt="Image" />
                                                        <div className="mask-icon">
                                                            <ul>
                                                            <li>
                                                                <a href="#" data-toggle="tooltip" data-placement="right" title="View">
                                                                <FaEye />
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare">
                                                                <FaSyncAlt />
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist">
                                                                <FaHeart />
                                                                </a>
                                                            </li>
                                                            </ul>
                                                            <a className="cart" href="#">
                                                            Add to Cart
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <Card.Body className="why-text">
                                                        <Card.Title>Lorem ipsum dolor sit amet</Card.Title>
                                                        <Card.Text>
                                                            <h5>$9.79</h5>
                                                        </Card.Text>
                                                    </Card.Body>
                                                </Card>
                                            </Col>
                                            <Col sm={6} md={6} lg={4} xl={4}>
                                                <Card className="products-single">
                                                    <div className="box-img-hover">
                                                        <div className="type-lb">
                                                            <p className="sale">Sale</p>
                                                        </div>
                                                        <Card.Img src="images/img-pro-01.jpg" fluid alt="Image" />
                                                        <div className="mask-icon">
                                                            <ul>
                                                            <li>
                                                                <a href="#" data-toggle="tooltip" data-placement="right" title="View">
                                                                <FaEye />
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare">
                                                                <FaSyncAlt />
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist">
                                                                <FaHeart />
                                                                </a>
                                                            </li>
                                                            </ul>
                                                            <a className="cart" href="#">
                                                            Add to Cart
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <Card.Body className="why-text">
                                                        <Card.Title>Lorem ipsum dolor sit amet</Card.Title>
                                                        <Card.Text>
                                                            <h5>$9.79</h5>
                                                        </Card.Text>
                                                    </Card.Body>
                                                </Card>
                                            </Col>
                                            <Col sm={6} md={6} lg={4} xl={4}>
                                                <Card className="products-single">
                                                    <div className="box-img-hover">
                                                        <div className="type-lb">
                                                            <p className="sale">Sale</p>
                                                        </div>
                                                        <Card.Img src="images/img-pro-01.jpg" fluid alt="Image" />
                                                        <div className="mask-icon">
                                                            <ul>
                                                            <li>
                                                                <a href="#" data-toggle="tooltip" data-placement="right" title="View">
                                                                <FaEye />
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare">
                                                                <FaSyncAlt />
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist">
                                                                <FaHeart />
                                                                </a>
                                                            </li>
                                                            </ul>
                                                            <a className="cart" href="#">
                                                            Add to Cart
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <Card.Body className="why-text">
                                                        <Card.Title>Lorem ipsum dolor sit amet</Card.Title>
                                                        <Card.Text>
                                                            <h5>$9.79</h5>
                                                        </Card.Text>
                                                    </Card.Body>
                                                </Card>
                                            </Col>
                                        </Row>
                                    </Tab>
                                    <Tab eventKey="tab-2" title={<><FaListUl /></>} id="grid-view" id="list-view">
                                        <div class="list-view-box">
                                            <Row>
                                                <Col sm={6} md={6} lg={4} xl={4}>
                                                    <div className="products-single fix">
                                                        <div className="box-img-hover">
                                                            <div className="type-lb">
                                                                <p className="new">New</p>
                                                            </div>
                                                            <img src="images/img-pro-01.jpg" className="img-fluid" alt="Image" />
                                                            <div className="mask-icon">
                                                                <ul>
                                                                <li>
                                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="View">
                                                                    <FaEye />
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="Compare">
                                                                    <FaSyncAlt />
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist">
                                                                    <FaHeart />
                                                                    </a>
                                                                </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col sm={6} md={6} lg={8} xl={8}>
                                                    <div className="why-text full-width">
                                                        <h4>Lorem ipsum dolor sit amet</h4>
                                                        <h5>
                                                            <del>$ 60.00</del> $40.79
                                                        </h5>
                                                        <p>
                                                            Integer tincidunt aliquet nibh vitae dictum. In turpis sapien, imperdiet quis magna nec, iaculis ultrices ante. Integer vitae suscipit nisi. Morbi dignissim risus sit amet orci porta, eget aliquam purus sollicitudin. Cras eu metus
                                                            felis. Sed arcu arcu, sagittis in blandit eu, imperdiet sit amet eros. Donec accumsan nisi purus, quis euismod ex volutpat in. Vestibulum eleifend eros ac lobortis aliquet. Suspendisse at ipsum vel lacus vehicula blandit et
                                                            sollicitudin quam. Praesent vulputate semper libero pulvinar consequat. Etiam ut placerat lectus.
                                                        </p>
                                                        <a className="btn hvr-hover" href="#">
                                                            <FaShoppingCart /> Add to Cart
                                                        </a>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                        <div class="list-view-box">
                                            <Row>
                                                <Col sm={6} md={6} lg={4} xl={4}>
                                                    <div className="products-single fix">
                                                        <div className="box-img-hover">
                                                            <div className="type-lb">
                                                                <p className="new">New</p>
                                                            </div>
                                                            <img src="images/img-pro-01.jpg" className="img-fluid" alt="Image" />
                                                            <div className="mask-icon">
                                                                <ul>
                                                                <li>
                                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="View">
                                                                    <FaEye />
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="Compare">
                                                                    <FaSyncAlt />
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist">
                                                                    <FaHeart />
                                                                    </a>
                                                                </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col sm={6} md={6} lg={8} xl={8}>
                                                    <div className="why-text full-width">
                                                        <h4>Lorem ipsum dolor sit amet</h4>
                                                        <h5>
                                                            <del>$ 60.00</del> $40.79
                                                        </h5>
                                                        <p>
                                                            Integer tincidunt aliquet nibh vitae dictum. In turpis sapien, imperdiet quis magna nec, iaculis ultrices ante. Integer vitae suscipit nisi. Morbi dignissim risus sit amet orci porta, eget aliquam purus sollicitudin. Cras eu metus
                                                            felis. Sed arcu arcu, sagittis in blandit eu, imperdiet sit amet eros. Donec accumsan nisi purus, quis euismod ex volutpat in. Vestibulum eleifend eros ac lobortis aliquet. Suspendisse at ipsum vel lacus vehicula blandit et
                                                            sollicitudin quam. Praesent vulputate semper libero pulvinar consequat. Etiam ut placerat lectus.
                                                        </p>
                                                        <a className="btn hvr-hover" href="#">
                                                            <FaShoppingCart /> Add to Cart
                                                        </a>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                        <div class="list-view-box">
                                            <Row>
                                                <Col sm={6} md={6} lg={4} xl={4}>
                                                    <div className="products-single fix">
                                                        <div className="box-img-hover">
                                                            <div className="type-lb">
                                                                <p className="new">New</p>
                                                            </div>
                                                            <img src="images/img-pro-01.jpg" className="img-fluid" alt="Image" />
                                                            <div className="mask-icon">
                                                                <ul>
                                                                <li>
                                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="View">
                                                                    <FaEye />
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="Compare">
                                                                    <FaSyncAlt />
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist">
                                                                    <FaHeart />
                                                                    </a>
                                                                </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col sm={6} md={6} lg={8} xl={8}>
                                                    <div className="why-text full-width">
                                                        <h4>Lorem ipsum dolor sit amet</h4>
                                                        <h5>
                                                            <del>$ 60.00</del> $40.79
                                                        </h5>
                                                        <p>
                                                            Integer tincidunt aliquet nibh vitae dictum. In turpis sapien, imperdiet quis magna nec, iaculis ultrices ante. Integer vitae suscipit nisi. Morbi dignissim risus sit amet orci porta, eget aliquam purus sollicitudin. Cras eu metus
                                                            felis. Sed arcu arcu, sagittis in blandit eu, imperdiet sit amet eros. Donec accumsan nisi purus, quis euismod ex volutpat in. Vestibulum eleifend eros ac lobortis aliquet. Suspendisse at ipsum vel lacus vehicula blandit et
                                                            sollicitudin quam. Praesent vulputate semper libero pulvinar consequat. Etiam ut placerat lectus.
                                                        </p>
                                                        <a className="btn hvr-hover" href="#">
                                                            <FaShoppingCart /> Add to Cart
                                                        </a>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Tab>
                                </Tabs>
                            </div>
                        </div>
                    </Col>  
                    <Col xl={3} lg={3} sm={12} xs={12} className="sidebar-shop-left">
                        <div className="product-categori">
                            <div className="search-product">
                                <Form action="#">
                                    <FormControl className="form-control" placeholder="Search here..." type="text" />
                                    <Button type="submit"> <FaSearch /> </Button>
                                </Form>
                            </div>
                        </div>
                        <div className="filter-sidebar-left">
                            <div className="title-left">
                                <h3>Categories</h3>
                            </div>
                        </div>
                        <div className="title-left">
                        <h3>Categories</h3>
                        </div>
                        <div className="list-group list-group-collapse list-group-sm list-group-tree" id="list-group-men" data-children=".sub-men">
                            <div className="list-group-collapse sub-men">
                                <a className="list-group-item list-group-item-action" href="#sub-men1" data-toggle="collapse" aria-expanded="true" aria-controls="sub-men1">
                                    Fruits & Drinks <small className="text-muted">(100)</small>
                                    <BsChevronDown />
                                </a>
                                <Collapse in={true} id="sub-men1">
                                <div className="list-group">
                                    <a href="#" className="list-group-item list-group-item-action active">
                                    Fruits 1 <small className="text-muted">(50)</small>
                                    </a>
                                    <a href="#" className="list-group-item list-group-item-action">
                                    Fruits 2 <small className="text-muted">(10)</small>
                                    </a>
                                    <a href="#" className="list-group-item list-group-item-action">
                                    Fruits 3 <small className="text-muted">(10)</small>
                                    </a>
                                    <a href="#" className="list-group-item list-group-item-action">
                                    Fruits 4 <small className="text-muted">(10)</small>
                                    </a>
                                    <a href="#" className="list-group-item list-group-item-action">
                                    Fruits 5 <small className="text-muted">(20)</small>
                                    </a>
                                </div>
                                </Collapse>
                            </div>
                            <div className="list-group-collapse sub-men">
                                <a className="list-group-item list-group-item-action" href="#sub-men2" data-toggle="collapse" aria-expanded="false" aria-controls="sub-men2">
                                Vegetables <small className="text-muted">(50)</small>
                                <BsChevronDown />
                                </a>
                                <Collapse id="sub-men2">
                                <div className="list-group">
                                    <a href="#" className="list-group-item list-group-item-action">
                                    Vegetables 1 <small className="text-muted">(10)</small>
                                    </a>
                                    <a href="#" className="list-group-item list-group-item-action">
                                    Vegetables 2 <small className="text-muted">(20)</small>
                                    </a>
                                    <a href="#" className="list-group-item list-group-item-action">
                                    Vegetables 3 <small className="text-muted">(20)</small>
                                    </a>
                                </div>
                                </Collapse>
                            </div>
                            <a href="#" className="list-group-item list-group-item-action">
                                Grocery <small className="text-muted">(150)</small>
                            </a>
                            <a href="#" className="list-group-item list-group-item-action">
                                Grocery <small className="text-muted">(11)</small>
                            </a>
                            <a href="#" className="list-group-item list-group-item-action">
                                Grocery <small className="text-muted">(22)</small>
                            </a>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>

        <FooterCarousel />
        <Footer />
    </div>
  )
}

export default SidebarShop