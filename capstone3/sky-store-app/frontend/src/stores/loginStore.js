import { create } from 'zustand';
import axios from 'axios';
import Swal from 'sweetalert2';

const loginStore = create((set) => ({
    loggedIn: null,
    loginForm: {
        email: "",
        password: "",
    },
    registerForm: {
        fullname: "",
        email: "",
        password: "",
        c_password: "",
    },
    updateLoginForm: (e) => {
        const { name, value } = e.target;
        set((state) => {
            return {
                loginForm: {
                    ...state.loginForm,
                    [name]: value,
                }
            }
        });
    },
    updateRegisterForm: (e) => {
        const { name, value } = e.target;
        set((state) => {
            return {
                registerForm: {
                    ...state.registerForm,
                    [name]: value,
                }
            }
        });
    },
    login: async () => {
        const { loginForm } = loginStore.getState();
        const res = axios.post("/login", loginForm);

        set({loggedIn: true});

        set({
            loginForm: {
                email: "",
                password: "",
            }
        });
    },
    register: async () => {
        const { registerForm } = loginStore.getState();
        
        if (registerForm.password !== registerForm.cpassword) {
          Swal.fire({
            title: 'Error!',
            text: 'Passwords do not match',
            icon: 'error',
            confirmButtonText: 'OK'
          });
          set({
            registerForm: {
              ...registerForm,
              password: "",
              cpassword: "",
            }
          });
          return;
        }
        
        await axios.post("/signup", registerForm);
        set({ loggedIn: true });
      
        set({
          registerForm: {
            fullname: "",
            email: "",
            password: "",
            cpassword: "",
          }
        });
    },   
    logout: async () => {
        await axios.get("/logout");
        set({loggedIn: false});
    },
    checkAuth: async () => {
        try {
            await axios.get("/check-auth");
            set({loggedIn: true});
        } catch (error) {
            set({loggedIn: false});
        }
    },
}))

export default loginStore;