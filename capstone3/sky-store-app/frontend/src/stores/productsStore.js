import { create } from 'zustand';
import axios from 'axios';

const productsStore = create((set) => ({
    products: null,
    createForm: {
        product_name: "",
        description: "",
        price: "",
        qty: "",
    },
    updateForm: {
        _id: null,
        product_name: "",
        description: "",
        price: "",
        qty: "",
    },
    getAllProduct: async () => {
        const res = await axios.get("/products");
        set({ products: res.data.products });
    },
    updateCreateForm: (e) => {
        const { name, value } = e.target;
        set((state) => {
            return {
                createForm: {
                    ...state.createForm,
                    [name]: value,
                }
            }
        });
    },
    createProduct: async (e) => {
        e.preventDefault();
        const { products, createForm } = productsStore.getState();
        const res = await axios.post("/products", createForm);
        const updatedProducts = Array.isArray(products) ? [...products, res.data.product] : [res.data.product];
        set({
            products: updatedProducts,
            createForm: {
                product_name: "",
                description: "",
                price: "",
                qty: "",
            },
        });
    },
    deleteProduct: async (_id) => {
        const res = await axios.put(`/products/archive/${_id}`);
        const {products} = productsStore.getState();
        const newProducts = products.filter((product) => {
            return product.isDeleted !== 1;
        });
        set({ products: newProducts })
    },
    handleUpdateFieldChange: (e) => {
        const { value, name } = e.target;
        set((state) => {
            return {
                updateForm: {
                    ...state.updateForm,
                    [name]: value,
                }
            }
        });
    },
    toggleUpdatedProduct: ({_id,product_name,description,price,qty}) => {
        set({
            updateForm: {
                _id,product_name,description,price,qty
            }
        })
    },
    updateProductData: async (e) => {
    
        e.preventDefault();
        const { 
            updateForm: {
                product_name, 
                description, 
                price, 
                qty, 
                _id
            },
            products,
         } = productsStore.getState();
        const res = await axios.put(`/products/${_id}`, {
            product_name,
            description,
            price,
            qty
        });
        
        const newProducts = [...products];
        const productIndex = products.findIndex((product) => {
        return product._id === _id;
        });
        newProducts[productIndex] = res.data.product;
        set({
            products: newProducts,
            updateForm: {
                _id: null,
                product_name: "",
                description: "",
                price: "",
                qty: "",
            }
        });
    }
}))

export default productsStore;