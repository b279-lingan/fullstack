const { mongo, default: mongoose } = require("mongoose");

const productSchema = new mongoose.Schema({
    product_name : {
		type : String
	},
    description : {
		type : String
	},
    price : {
		type : Number,
		default: 0
	},
    qty : {
		type : Number,
		default: 0
	},
    image_url : {
		type : String,
		default: ""
	},
    isAvailable : {
		type : Boolean,
		default: 0
	},
    isDeleted : {
		type : Boolean,
		default: 0
	},
    created_at : {
        type : Date,
        default : new Date()
    },
});

const Product = mongoose.model("Product", productSchema);
module.exports = Product;