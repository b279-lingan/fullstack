if(process.env.NODE_ENV != "production") {
    require("dotenv").config();
}

const express = require("express");
const cors = require("cors")
const cookieParser = require("cookie-parser");
const app = express();
const multer = require('multer');

const database = require("./config/database");
const requireAuth = require("./middleware/requireAuth");
const path = require('path');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, images);
    },
    filename: (req, file, cb) => {
        console.log(file);
        cb(null, Date.now() + path.extname(file.originalname));
    }
});
const upload = multer({storage: storage});

database();

app.use(express.json());
app.use(cookieParser());
app.use(
    cors({
        origin: true,
        credentials: true,
    })
);

const productController = require("./controllers/productController");
const userController = require("./controllers/userController");

app.post("/signup", userController.signup);
app.post("/login", userController.login);
app.get("/logout", userController.logout);

app.get("/check-auth", requireAuth, userController.checkAuth);
app.post("/products", requireAuth, productController.createProduct);
app.get("/products", requireAuth, productController.getAllProduct);
app.get("/products/:id", requireAuth, productController.getOneProduct);
app.put("/products/:id", requireAuth, productController.updateProduct);
app.put("/products/archive/:id", requireAuth, productController.deleteProduct);

app.listen(process.env.PORT);