let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}

function enqueue(item){
    // add an item

    collection.push(item)
    return collection;
}


function dequeue(){
    // remove an item

    let newCollection = []
        let index = 0
        for (i = 1; i < collection.length; i++) {
            newCollection[index] = collection [i]
            index++
        }
        collection = [...newCollection];
        return collection

}

function front(){
    // get the front item

    let firstItem = collection[0];
    return firstItem;

}

function size(){
    // get the size of the array

    let size = collection.length;
    return size;

}

function isEmpty(){
    // check array if empty

    if (collection.length === 0) {
            return true
        } else {
            return false
        }

}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};